"""
models for db migrations and use entities in app
author: z.raddani
create at: 2022-08
"""

from main import db
from passlib.apps import custom_app_context as pwd_context
from itsdangerous import (TimedJSONWebSignatureSerializer
                          as Serializer, BadSignature, SignatureExpired)

class Project(db.Model):
    __tablename__ = 'projects'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(128))
    tasks = db.relationship('Task', backref='projects')

class Task(db.Model):
    __tablename__ = 'tasks'
    id = db.Column(db.Integer, primary_key=True)
    project_id  = db.Column(db.Integer,db.ForeignKey('projects.id'))
    title = db.Column(db.String(128))
    description = db.Column(db.String(500))
    users = db.relationship('DeveloperTasks', backref='tasks')


class User(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key = True)
    username = db.Column(db.String(32), index = True)
    password_hash = db.Column(db.String(128))

    def hash_password(self, password):
        self.password_hash = pwd_context.encrypt(password)

    def verify_password(self, password):
        return pwd_context.verify(password, self.password_hash)

    def generate_auth_token(self, secret, expiration = 600):
        s = Serializer('', expires_in = expiration)
        return s.dumps({ 'id': self.id })

class Token(db.Model):
    __tablename__ = 'tokens'
    id = db.Column(db.Integer, primary_key=True)
    user_id  = db.Column(db.Integer,db.ForeignKey('users.id'))
    token = db.Column(db.String(500))
    expired_at = db.Column(db.DateTime)


class UserRole(db.Model):
    __tablename__ = 'user_roles'
    id = db.Column(db.Integer(), primary_key=True)
    user_id = db.Column(db.Integer(), db.ForeignKey('users.id', ondelete='CASCADE'))
    role = db.Column(db.String(50))

class Developer(db.Model):
    __tablename__ = 'developers'
    id = db.Column(db.Integer(), primary_key=True)
    user_id = db.Column(db.Integer(), db.ForeignKey('users.id', ondelete='CASCADE'))
    user = db.relationship('User', uselist=False,backref='developers')

class DeveloperProjects(db.Model):
    __tablename__ = 'developer_projects'
    id = db.Column(db.Integer(), primary_key=True)
    developer_id = db.Column(db.Integer(), db.ForeignKey('developers.id'))
    project_id = db.Column(db.Integer(), db.ForeignKey('projects.id'))

class DeveloperTasks(db.Model):
    __tablename__ = 'developer_tasks'
    id = db.Column(db.Integer(), primary_key=True)
    developer_id = db.Column(db.Integer(), db.ForeignKey('developers.id'))
    task_id = db.Column(db.Integer(), db.ForeignKey('tasks.id'))


class PManager(db.Model):
    __tablename__ = 'pmanagers'
    id = db.Column(db.Integer(), primary_key=True)
    user_id = db.Column(db.Integer(), db.ForeignKey('users.id', ondelete='CASCADE'))

class PManagerProjects(db.Model):
    __tablename__ = 'pmanager_projects'
    id = db.Column(db.Integer(), primary_key=True)
    pmanager_id = db.Column(db.Integer(), db.ForeignKey('pmanagers.id'))
    project_id = db.Column(db.Integer(), db.ForeignKey('projects.id'))