FROM continuumio/miniconda3:4.10.3-alpine

WORKDIR /home/conda_flask_docker

COPY . .

RUN conda env create -f environment.yml

RUN echo "source activate communer" > ~/.bashrc
ENV PATH /opt/conda/envs/app/bin:$PATH

EXPOSE 5000
RUN chmod +x run.sh
ENTRYPOINT ["./run.sh"]
