#!/bin/sh

export FLASK_APP=main.py
flask db init
flask db migrate
flask db upgrade

DIR="$(dirname "${VAR}")"
mkdir /var/log/gunicorn
exec gunicorn -c $DIR/gunicorn_conf.py main:main

# exec gunicorn -b :5000 --access-logfile - --error-logfile - api:app
