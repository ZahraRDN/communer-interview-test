"""
resources for convert models to api response resources
author: z.raddani
create at: 2022-08
"""
from main import api
from flask_restx import fields

user = api.model('User', {
    'id' : fields.Integer,
    'username' : fields.String
})

developer = api.model('Developer', {
    'id': fields.Integer,
    # 'user' : fields.Url('user', attribute='username')
})

task = api.model('Task', {
    'id': fields.Integer,
    'title': fields.String,
    'description': fields.String
})

task_by_user = api.model('Task', {
    'id': fields.Integer,
    'title': fields.String,
    'description': fields.String,
    'users': fields.List(fields.Nested(developer)),
})