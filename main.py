"""
main file to start app
author: z.raddani
create at: 2022-08
"""
from flask import Flask, request, abort, g, make_response
# from flask import jsonify
import json
from datetime import  datetime, timedelta
from flask_httpauth import HTTPTokenAuth
import sys
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_restx import Resource, Api, marshal


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///app.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.secret_key = '\xfd{H\xe5<\x95\xf9\xe3\x96.5\xd1\x01O<!\xd5\xa2\xa0\x9fR"\xa1\xa8'

# roles exist in app
ROLES = {'PMANAGER' : 'p_manager', 'DEVELOPER' : 'developer' }

# roles that assigned for endpoints
path_roles = {
    '/' : [ROLES['PMANAGER']],  ## for test
    'task_assign' : [ROLES['PMANAGER']],
    'task_list_by_user' : [ROLES['PMANAGER']],
    'task_list' : [ROLES['DEVELOPER']],
} # other path are public

db = SQLAlchemy(app)
from models import *
db.init_app(app)
migrate = Migrate(app, db)
auth = HTTPTokenAuth('Bearer')
api = Api(app)

from resources import task, task_by_user

# signup with username, password, role
@app.route("/signup", methods = ['POST'])
def signup():
    # get request params
    username = request.json.get('username')
    password = request.json.get('password')
    role = request.json.get('role')

    # check params exist
    if username is None or password is None:
        return make_response(
            {
                'status' : False,
                'message': 'missing argument'
            },
            400
        )
    if User.query.filter_by(username = username).first() is not None: # check username is valid and exist
        return make_response(
            {
                'status' : False,
                'message': 'existing user'
            },
            400
        )

    # add user
    user = User(username = username)
    user.hash_password(password)
    db.session.add(user)
    db.session.commit()

    # add role for user
    user_role = UserRole(user_id = user.id,role = role)
    db.session.add(user_role)

    # add developer if role is developer
    if role == 'developer':
        developer = Developer(user_id = user.id)
        db.session.add(developer)

    # add product manager if role is p_manager
    if role == 'p_manager':
        pmanager = PManager(user_id = user.id)
        db.session.add(pmanager)

    db.session.commit()

    # api response resource
    return make_response(
            {
                'status' : True,
                'message': 'user created successfully',
                'data': {
                 'username': user.username,
                 'role' : role,
                 'id': user.id
                }
            },
            200
        )

# generate and store token for the user
def get_auth_token():
    value = g.user.generate_auth_token(app.config["SECRET_KEY"])
    token = Token(user_id = g.user.id, token = value.decode('ascii'), expired_at = datetime.now()+timedelta(days = 1))
    db.session.add(token)
    db.session.commit()

    # api response resource
    return make_response(
            {
                'status' : True,
                'message': 'token created successfully',
                'data': {
                 'token': value.decode('ascii') 
                }
            },
            200
        )

# check username and password is valid  
def verify_password(username, password):
    user = User.query.filter_by(username = username).first()
    if not user or not user.verify_password(password):
        return False
    g.user = user
    return True

# login api that response needs for other apis
@app.route('/signin', methods =['POST'])
def login():
    auth = request.json
    if not auth or not auth.get('username') or not auth.get('password'):
        # returns 401 if any username or / and password is missing
        return make_response(
            'Could not verify',
            401,
            {'WWW-Authenticate' : 'Basic realm ="Login required !!"'}
        )
  
    if verify_password(auth.get('username'),auth.get('password')): # check username and password is valid
        return get_auth_token() # generate and store token

# check token and permision for each api
@auth.verify_token
def verify_token(token):
    # print(request.endpoint)
    if request.endpoint in path_roles:
        selected_roles = path_roles[request.endpoint]
        res = Token.query.filter(Token.token == token, Token.expired_at > datetime.now()).all()
        if len(res) > 0:
            g.user_id = res[0].user_id
            u_roles = UserRole.query.filter(UserRole.user_id == res[0].user_id).all()
            for u_r in u_roles:
                if u_r.role in selected_roles:
                    return True
    return False

# ###### Global #######
@app.route("/")
@auth.login_required
def home():
    return "<h1> Hi Communer User .... </h1>"

# #### Developer  #######
# all tasks in own project
@api.route("/project/<project_id>/task")
class TaskList(Resource):
    @auth.login_required
    def get(self, project_id):   

        # check project is valid in db
        if Project.query.filter_by(id = project_id).first() is None:
            return make_response(
                {
                    'status' : False,
                    'message': 'project not exist'
                },
                400
            )
        tasks = Task.query.filter(Task.project_id == project_id).all() # find tasks for the project

        # api response
        return make_response(
            {
                'status' : True,
                'message': 'project tasks',
                'data': marshal(tasks, task)
            },
            200
        )

# ##### Product Manager #####
# assign task to a developer by p_manager
@app.route("/task/assign", methods =['POST'])
@auth.login_required
def task_assign():
    developer_id = request.json.get('developer_id')
    task_id = request.json.get('task_id')

    # check params are exist
    if developer_id is None or task_id is None:
        return make_response(
            {
                'status' : False,
                'message': 'missing argument'
            },
            400
        )

    # check params are valid in db
    if Task.query.filter_by(id = task_id).first() is None:
        return make_response(
            {
                'status' : False,
                'message': 'task not exist'
            },
            400
        )

    task = Task.query.filter_by(id = task_id).first() # find task
    p_manager = PManager.query.filter_by(user_id = g.user_id).first() # find product manager by token in request

    # check product manager has permision to project
    if PManagerProjects.query.filter_by(id = task.project_id, pmanager_id = p_manager.id).first() is None:
        return make_response(
            {
                'status' : False,
                'message': 'project no permision'
            },
            400
        )
    
    # check param developer exist in db
    if Developer.query.filter_by(id = developer_id).first() is None:
        return make_response(
            {
                'status' : False,
                'message': 'developer not exist'
            },
            400
        )

    # check developer assign the task before ?
    if DeveloperTasks.query.filter_by(id = task_id, developer_id = developer_id).count() > 0:
        return make_response(
            {
                'status' : False,
                'message': 'assigned before'
            },
            400
        )

    # assign task to developer
    d_project = DeveloperTasks(developer_id = developer_id, task_id = task_id)
    db.session.add(d_project)
    db.session.commit()

    # api response
    return make_response(
            {
                'status' : True,
                'message': 'project assigned to developer successfully',
                'data': {}
            },
            200
        )

# get list of tasks with their users for a project
# call by p_manager
@api.route("/project/<project_id>/task/<developer_id>")
class TaskListByUser(Resource):
    @auth.login_required
    def get(self, project_id, developer_id):   

        # check project is valid in db
        if Project.query.filter_by(id = project_id).first() is None:
            return make_response(
                {
                    'status' : False,
                    'message': 'project not exist'
                },
                400
            )

        tasks = Task.query.filter(Task.project_id == project_id, Task.users.any(developer_id = developer_id) ).all() # find tasks for the project

        # api response
        return make_response(
            {
                'status' : True,
                'message': 'project tasks',
                'data': marshal(tasks, task)
            },
            200
        )

if __name__ == '__main__':
    app.run(debug=True)

