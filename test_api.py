"""
test apis
To do: implement for all functions
author: z.raddani
create at: 2022-08
"""
import unittest
import json

from main import app

# python -m unittest test_api.py

class SignupTest(unittest.TestCase):

    def setUp(self):
        self.app = app.test_client()

    def test_successful_signup(self):

        payload = json.dumps({
            "username": "test5",
            "password": "test5",
            "role" : "p_manager"
        })

        response = self.app.post('/signup', headers={"Content-Type": "application/json"}, data=payload)

        self.assertEqual(str, type(response.json['data']['username']))
        self.assertEqual(200, response.status_code)

    def tearDown(self):
        # Delete Database datas after the test is complete
        # ....
        # ToDo ....
        pass